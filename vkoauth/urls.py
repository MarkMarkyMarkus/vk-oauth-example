from django.urls import path, include

urlpatterns = [
    path('api/', include('vkoauth.user.urls')),
    path('', include('frontend.urls')),
]
