import os
import django_heroku

from pathlib import Path

from vkoauth import user

BASE_DIR = Path(__file__).resolve().parent.parent

SECRET_KEY = 'admin'

VK_APP_ID = os.getenv('VK_CLIENT_ID', 0000000)
VK_SECRET = os.getenv('VK_SECRET', 'secret')
REDIRECT_URL = os.getenv('REDIRECT_URL', 'http://localhost:8000')

DEBUG = True

ALLOWED_HOSTS = []

INSTALLED_APPS = [
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.staticfiles',
    'rest_framework',
    'vkoauth.user',
    'frontend',
]

MIDDLEWARE = []

DATABASES = {}

ROOT_URLCONF = 'vkoauth.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, 'templates')]
        ,
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'vkoauth.wsgi.application'

LANGUAGE_CODE = 'en-us'
TIME_ZONE = 'UTC'
USE_I18N = True
USE_L10N = True
USE_TZ = True

STATIC_URL = '/static/'

django_heroku.settings(locals())
