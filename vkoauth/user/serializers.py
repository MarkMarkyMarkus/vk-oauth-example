from rest_framework import serializers


class VkUserFriendsSerializer(serializers.Serializer):
    id = serializers.FloatField()
    first_name = serializers.CharField()
    last_name = serializers.CharField()
    photo = serializers.CharField(source="photo_50")


class VkUserSerializer(serializers.Serializer):
    id = serializers.FloatField()
    first_name = serializers.CharField()
    last_name = serializers.CharField()
    photo = serializers.CharField()
