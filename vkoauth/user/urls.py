from django.urls import path
from .views import VkUserView

app_name = "users"

urlpatterns = [
    path('user/', VkUserView.as_view()),
    path('code/', VkUserView.as_view()),
]
