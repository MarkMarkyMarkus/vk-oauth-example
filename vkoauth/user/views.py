from rest_framework.response import Response
from rest_framework.views import APIView

from .VkAPIHelper import *
from .serializers import VkUserSerializer, VkUserFriendsSerializer


class VkUserView(APIView):
    def post(self, request):
        user_id = request.data.get("user_id")
        token = request.data.get("access_token")
        try:
            user = get_user(token)
            friends = get_friends(user_id, token)
        except vk_api.AuthError as error_msg:
            return Response({"error": str(error_msg)})
        user_serializer = VkUserSerializer(user)
        friends_serializer = VkUserFriendsSerializer(friends, many=True)
        return Response({"profile": {"user": user_serializer.data, "friends": friends_serializer.data}})

    def get(self, request):
        try:
            auth_response = code_auth(request.query_params.get("code"))
        except vk_api.AuthError as error_msg:
            return Response({"error": str(error_msg)})
        return Response(auth_response)
