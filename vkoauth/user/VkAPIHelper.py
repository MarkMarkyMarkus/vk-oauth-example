import vk_api
from django.conf import settings

app = settings.VK_APP_ID
secret = settings.VK_SECRET
redirect_url = settings.REDIRECT_URL


def code_auth(code: str) -> any:
    return vk_api.VkApi(app_id=app, client_secret=secret).code_auth(code, redirect_url)


def get_friends(user_id: int, token: str) -> any:
    vk = vk_api.VkApi(token=token).get_api()
    friends = vk.friends.get(
        user_id=user_id,
        order="random",
        count=5,
        fields=["photo_50"],
        access_token=token
    )
    return friends['items']


def get_user(token: str) -> any:
    vk = vk_api.VkApi(token=token).get_api()
    user = vk.users.get(
        fields=["photo"]
    )
    return user[0]
