export function setUser(user) {
    return {
        type: 'SET_USER',
        user
    }
}

export function setProfile(profile) {
    return {
        type: 'SET_PROFILE',
        profile
    }
}
