import React from 'react'
import {render} from 'react-dom'
import {Provider} from 'react-redux'
import {applyMiddleware, createStore} from 'redux'
import rootReducer from './reducers'
import thunk from "redux-thunk";
import AppContainer from "./containers/App";

const persistedState = localStorage.getItem('vk_user')
    ?
    {user: JSON.parse(localStorage.getItem('vk_user'))}
    :
    {}

const store = createStore(
    rootReducer,
    persistedState,
    applyMiddleware(thunk)
)

render(
    <Provider store={store}>
        <AppContainer/>
    </Provider>,
    document.getElementById('app')
)
