export function getToken(codeQuery) {
    return fetch(`${API_URL}/code/${codeQuery}`)
        .then(response => {
            return response.json();
        })
        .then(json => {
            if (json.error === undefined) {
                return json;
            } else throw new Error(json.error);
        })
        .catch(e => console.error(e));
}

export function getProfile(user) {
    return fetch(
        `${API_URL}/user/`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({access_token: user.access_token, user_id: user.user_id})
        }
    )
        .then(response => {
            return response.json();
        })
        .then(json => {
            if (json.error === undefined) {
                return json.profile;
            } else throw new Error(json.error);
        })
        .catch(e => console.error(e));
}
