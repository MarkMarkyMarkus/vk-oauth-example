import {connect} from 'react-redux';
import UserProfile from "../components/UserProfile";
import {setProfile, setUser} from "../actions";

const mapStateToProps = state => ({
    user: state.user,
    profile: state.profile
});

const mapDispatchToProps = dispatch => ({
    setProfile: profile => dispatch(setProfile(profile)),
    logout: () => {
        localStorage.removeItem("vk_user");
        dispatch(setUser({user_id: -1, access_token: null}));
    }
})

const userProfileContainer = connect(mapStateToProps, mapDispatchToProps)(UserProfile);

export default userProfileContainer;
