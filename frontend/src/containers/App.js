import {connect} from 'react-redux';
import App from '../components/App';

const mapStateToProps = state => ({
    user: state.user
});

const AppContainer = connect(mapStateToProps)(App);

export default AppContainer;
