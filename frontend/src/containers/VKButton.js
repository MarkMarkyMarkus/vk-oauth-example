import React from 'react'
import {connect} from 'react-redux'
import styles from "../styles/VK.module.scss";
import VkLogo from "../image/vk-1.svg";

const handleRedirect = () => {
    // TODO: Should be on the SERVER side...
    window.location.href =
        `https://oauth.vk.com/authorize?client_id=${VK_CLIENT_ID}&display=popup&redirect_uri=${REDIRECT_URL}&scope=friends&response_type=code&v=5.122`;
}

const VKButton = () => {
    return (
        <div className={styles["vk-button"]}>
            <button className={styles["vk-button__input"]} onClick={handleRedirect}>
                <VkLogo
                    className={styles["vk-button__input-icon"]}
                />
                VK Auth
            </button>
        </div>
    )
}

export default connect()(VKButton)
