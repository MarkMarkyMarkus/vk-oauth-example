import React, {Component} from 'react';
import {getProfile} from "../util/APIUtils";
import styles from "../styles/VK.module.scss";

class UserProfile extends Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        if (this.props.user.access_token !== null)
            getProfile(this.props.user)
                .then(profile => this.props.setProfile(profile))
    }

    render() {
        const {profile} = this.props;

        return (
            profile ?
                <div align="center" className={styles["my-profile"]}>
                    <div>
                        <img src={profile.user.photo} alt={`${profile.user.first_name}`} onClick={this.props.logout}/>
                        <h1>{`${profile.user.first_name}`}</h1>
                    </div>
                    <div className={styles["my-friends"]}>
                        {profile.friends.map(
                            (friend) =>
                                <div className={styles["my-friend"]} key={friend.id}>
                                    <img src={friend.photo} alt={`${friend.first_name}`}/>
                                    <h3>{friend.first_name}</h3>
                                </div>
                        )}
                    </div>
                </div>
                : <h1 align="center">Nothing to show :(</h1>
        )
    }
}

export default UserProfile;
