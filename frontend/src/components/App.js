import React, {Component} from 'react'
import VKButton from '../containers/VKButton'
import UserProfileContainer from "../containers/UserProfile";
import {setUser} from "../actions";
import {getToken} from "../util/APIUtils";

class App extends Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        if (this.props.user.access_token === null && location.search.length > 0)
            getToken(location.search)
                .then(vkUser => {
                        if (vkUser) {
                            const user = {
                                user_id: vkUser.user_id,
                                access_token: vkUser.access_token,
                            }
                            // TODO: An unsafe way to store data / credentials!
                            localStorage.setItem('vk_user', JSON.stringify(user));
                            this.props.dispatch(setUser(user));
                        }
                    }
                )
    }

    render() {
        return (
            this.props.user.access_token ?
                <UserProfileContainer/>
                :
                <VKButton/>
        )
    }
}

export default App
