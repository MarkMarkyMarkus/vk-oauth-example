const profile = (state = null, action) => {
    switch (action.type) {
        case 'SET_PROFILE':
            return action.profile
        default:
            return state
    }
}

export default profile
