const defaultUser = {
    user_id: -1,
    access_token: null
}

const user = (state = defaultUser, action) => {
    switch (action.type) {
        case 'SET_USER':
            return action.user
        default:
            return state
    }
}

export default user
