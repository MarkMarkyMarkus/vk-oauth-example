const webpack = require('webpack');

module.exports = {
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: "babel-loader"
                }
            },
            {
                test: /\.s[ac]ss$/i,
                use: [
                    'style-loader',
                    'css-loader',
                    'sass-loader',
                ],
            },
            {
                test: /\.svg$/,
                loader: 'react-svg-loader'
            }
        ]
    },
    plugins: [
        new webpack.DefinePlugin({
            API_URL: JSON.stringify(process.env.API_URL),
            VK_CLIENT_ID: JSON.stringify(process.env.VK_CLIENT_ID),
            REDIRECT_URL: JSON.stringify(process.env.REDIRECT_URL),
        })
    ],
};
